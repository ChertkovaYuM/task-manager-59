package ru.tsc.chertkova.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.chertkova.tm.api.repository.dto.IUserDtoRepository;
import ru.tsc.chertkova.tm.api.service.IPropertyService;
import ru.tsc.chertkova.tm.api.service.dto.IUserDtoService;
import ru.tsc.chertkova.tm.dto.model.UserDTO;
import ru.tsc.chertkova.tm.exception.entity.UserNotFoundException;
import ru.tsc.chertkova.tm.exception.field.EmailEmptyException;
import ru.tsc.chertkova.tm.exception.field.EmailExistsException;
import ru.tsc.chertkova.tm.exception.field.IdEmptyException;
import ru.tsc.chertkova.tm.exception.field.NameEmptyException;
import ru.tsc.chertkova.tm.exception.user.LoginEmptyException;
import ru.tsc.chertkova.tm.exception.user.LoginExistsException;
import ru.tsc.chertkova.tm.exception.user.PasswordEmptyException;
import ru.tsc.chertkova.tm.util.HashUtil;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class UserDtoService extends AbstractDtoService<UserDTO> implements IUserDtoService {

    @NotNull
    @Autowired
    private IUserDtoRepository repository;

    @NotNull
    @Autowired
    private final IPropertyService propertyService;

    @NotNull
    public IUserDtoRepository getRepository() {
        return repository;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public UserDTO findByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        @NotNull final IUserDtoRepository userRepository = getRepository();
        @Nullable UserDTO user = userRepository.findByLogin(login);
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public UserDTO findByEmail(@Nullable final String email) {
        Optional.ofNullable(email).orElseThrow(EmailEmptyException::new);
        @NotNull final IUserDtoRepository userRepository = getRepository();
        @Nullable UserDTO user = userRepository.findByEmail(email);
        return user;
    }

    @Override
    @Transactional
    public boolean existsById(@Nullable String id) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final IUserDtoRepository userRepository = getRepository();
        int count = userRepository.existsById(id);
        return count > 0;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public UserDTO remove(@Nullable final UserDTO user) {
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        @NotNull final IUserDtoRepository userRepository = getRepository();
        Optional.ofNullable(userRepository.findById(user.getId()))
                .orElseThrow(UserNotFoundException::new);
        removeById(user.getId());
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public UserDTO removeByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        @NotNull final IUserDtoRepository userRepository = getRepository();
        UserDTO user = Optional.ofNullable(userRepository.findByLogin(login))
                .orElseThrow(UserNotFoundException::new);
        removeById(user.getId());
        return user;
    }

    @Override
    @Nullable
    @Transactional
    public UserDTO removeById(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final IUserDtoRepository userRepository = getRepository();
        @Nullable UserDTO user = userRepository.findById(id);
        userRepository.removeById(id);
        return user;
    }

    @Override
    @Transactional
    public void clear(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final IUserDtoRepository userRepository = getRepository();
        userRepository.clear();
    }

    @Override
    @SneakyThrows
    @Transactional
    public boolean isLoginExists(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        @NotNull final IUserDtoRepository userRepository = getRepository();
        long count = userRepository.isLoginExist(login);
        return count > 0;
    }

    @Override
    @SneakyThrows
    @Transactional
    public boolean isEmailExists(@Nullable final String email) {
        Optional.ofNullable(email).orElseThrow(EmailEmptyException::new);
        @NotNull final IUserDtoRepository userRepository = getRepository();
        long count = userRepository.isEmailExist(email);
        return count > 0;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public UserDTO setPassword(@Nullable final String userId,
                            @Nullable final String password) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(password).orElseThrow(PasswordEmptyException::new);
        @NotNull final IUserDtoRepository userRepository = getRepository();
        @Nullable UserDTO user;
        Optional.ofNullable(userRepository.findById(userId)).orElseThrow(UserNotFoundException::new);
        userRepository.setPassword(userId, HashUtil.salt(propertyService, password));
        user = userRepository.findById(userId);
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public UserDTO updateUser(@Nullable final String userId,
                           @Nullable final String firstName,
                           @Nullable final String lastName,
                           @Nullable final String middleName) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(firstName).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(middleName).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(lastName).orElseThrow(NameEmptyException::new);
        @NotNull final IUserDtoRepository userRepository = getRepository();
        @Nullable UserDTO user;
        user = Optional.ofNullable(userRepository.findById(userId))
                .orElseThrow(UserNotFoundException::new);
        userRepository.update(user);
        user = userRepository.findById(userId);
        return user;
    }

    @Nullable
    @Override
    @Transactional
    public UserDTO lockUserByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        @NotNull final IUserDtoRepository userRepository = getRepository();
        @Nullable UserDTO user;
        Optional.ofNullable(userRepository.findByLogin(login)).orElseThrow(UserNotFoundException::new);
        userRepository.setLockedFlag(login, true);
        user = userRepository.findByLogin(login);
        return user;
    }

    @Override
    @Nullable
    @Transactional
    public UserDTO unlockUserByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        @NotNull final IUserDtoRepository userRepository = getRepository();
        Optional.ofNullable(userRepository.findByLogin(login)).orElseThrow(UserNotFoundException::new);
        userRepository.setLockedFlag(login, false);
        @Nullable UserDTO user = userRepository.findByLogin(login);
        return user;
    }

    @Override
    @Nullable
    @Transactional
    public UserDTO add(@Nullable final UserDTO user) {
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(user.getLogin()).orElseThrow(LoginEmptyException::new);
        Optional.ofNullable(user.getEmail()).orElseThrow(EmailEmptyException::new);
        Optional.ofNullable(user.getPasswordHash()).orElseThrow(PasswordEmptyException::new);
        @NotNull final IUserDtoRepository userRepository = getRepository();
        if (userRepository.isLoginExist(user.getLogin()) > 0) throw new LoginExistsException();
        if (userRepository.isEmailExist(user.getEmail()) > 0) throw new EmailExistsException();
        userRepository.add(user);
        return user;
    }

    @Override
    @Nullable
    @Transactional
    public UserDTO updateById(@Nullable final String id,
                           @Nullable final String firstName,
                           @Nullable final String middleName,
                           @Nullable final String lastName) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(firstName).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(middleName).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(lastName).orElseThrow(NameEmptyException::new);
        @NotNull final IUserDtoRepository userRepository = getRepository();
        @Nullable UserDTO user = Optional.ofNullable(userRepository.findById(id))
                .orElseThrow(UserNotFoundException::new);
        userRepository.update(user);
        user = userRepository.findById(id);
        return user;
    }

    @Override
    @Nullable
    @Transactional
    public UserDTO findById(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final IUserDtoRepository userRepository = getRepository();
        @Nullable UserDTO user = userRepository.findById(id);
        return user;
    }

    @Override
    @Transactional
    public int getSize(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final IUserDtoRepository userRepository = getRepository();
        @Nullable int count = userRepository.getSize();
        return count;
    }

    @Override
    @Nullable
    @Transactional
    public List<UserDTO> findAll(@Nullable String id) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final IUserDtoRepository userRepository = getRepository();
        @Nullable List<UserDTO> users = userRepository.findAll();
        return users;
    }

    @Override
    @Nullable
    @Transactional
    public List<UserDTO> addAll(@Nullable List<UserDTO> users) {
        Optional.ofNullable(users).orElseThrow(UserNotFoundException::new);
        @NotNull final IUserDtoRepository userRepository = getRepository();
        for (UserDTO user : users) {
            Optional.ofNullable(user.getLogin()).orElseThrow(LoginEmptyException::new);
            Optional.ofNullable(user.getEmail()).orElseThrow(EmailEmptyException::new);
            Optional.ofNullable(user.getPasswordHash()).orElseThrow(PasswordEmptyException::new);
            userRepository.add(user);
        }
        return users;
    }

    @Override
    @Nullable
    @Transactional
    public List<UserDTO> removeAll(@Nullable final List<UserDTO> users) {
        Optional.ofNullable(users).orElseThrow(UserNotFoundException::new);
        @NotNull final IUserDtoRepository userRepository = getRepository();
        for (UserDTO u : users) {
            userRepository.removeById(u.getId());
        }
        return users;
    }

}
