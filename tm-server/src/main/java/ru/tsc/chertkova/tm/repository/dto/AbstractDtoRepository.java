package ru.tsc.chertkova.tm.repository.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.chertkova.tm.api.repository.dto.IAbstractDtoRepository;
import ru.tsc.chertkova.tm.dto.model.AbstractModelDTO;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Getter
@Repository
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractDtoRepository<M extends AbstractModelDTO>
        implements IAbstractDtoRepository<M> {

    @NotNull
    @PersistenceContext
    protected EntityManager entityManager;

    @Override
    public void add(@NotNull M model) {
        entityManager.merge(model);
    }

    @Override
    public abstract void clear();

    @Override
    @NotNull
    public abstract List<M> findAll();

    @Override
    @Nullable
    public abstract M findById(@NotNull String id);

    @Override
    public abstract int getSize();

    @Override
    public abstract void removeById(@NotNull String id);

    @Override
    public void update(@NotNull M model) {
        entityManager.merge(model);
    }

}
