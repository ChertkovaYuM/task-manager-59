package ru.tsc.chertkova.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.dto.model.ProjectDTO;

import java.util.List;

public interface IProjectDtoRepository extends IAbstractUserOwnerModelDtoRepository<ProjectDTO> {

    void add(@NotNull String userId,
             @NotNull ProjectDTO model);

    void clear(@NotNull String userId);

    @NotNull
    List<ProjectDTO> findAll(@NotNull String userId);

    @Nullable
    ProjectDTO findById(@NotNull String userId,
                        @NotNull String id);

    int getSize(@NotNull String userId);

    void removeById(@NotNull String userId,
                    @NotNull String id);

    void update(@NotNull String userId,
                @NotNull ProjectDTO model);

    void changeStatus(@NotNull String id,
                      @NotNull String userId,
                      @NotNull String status);

    int existsById(@Nullable String id);

}
