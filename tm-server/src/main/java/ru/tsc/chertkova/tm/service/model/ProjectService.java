package ru.tsc.chertkova.tm.service.model;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.chertkova.tm.api.repository.model.IProjectRepository;
import ru.tsc.chertkova.tm.api.repository.model.ITaskRepository;
import ru.tsc.chertkova.tm.api.service.model.IProjectService;
import ru.tsc.chertkova.tm.enumerated.Status;
import ru.tsc.chertkova.tm.exception.entity.*;
import ru.tsc.chertkova.tm.exception.field.*;
import ru.tsc.chertkova.tm.model.Project;
import ru.tsc.chertkova.tm.model.Task;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ProjectService extends AbstractUserOwnerService<Project> implements IProjectService {

    @NotNull
    @Autowired
    private IProjectRepository repository;

    @NotNull
    public IProjectRepository getRepository() {
        return repository;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Project add(@Nullable final Project project) {
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        Optional.ofNullable(project.getUser()).orElseThrow(UserNotFoundException::new);
        @NotNull final IProjectRepository projectRepository = getRepository();
        Optional.ofNullable(project.getName()).orElseThrow(NameEmptyException::new);
        projectRepository.add(project);
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Project updateById(@Nullable final String id,
                              @Nullable final String userId,
                              @Nullable final String name,
                              @Nullable final String description
    ) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).orElseThrow(DescriptionEmptyException::new);
        @NotNull final IProjectRepository projectRepository = getRepository();
        @Nullable Project project;
        project = Optional.ofNullable(findById(userId, id)).orElseThrow(ProjectNotFoundException::new);
        projectRepository.update(project);
        project = findById(userId, id);
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Project changeProjectStatusById(@Nullable final String userId,
                                           @Nullable final String id,
                                           @Nullable final Status status) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(status).orElseThrow(StatusNotFoundException::new);
        @NotNull final IProjectRepository projectRepository = getRepository();
        @Nullable Project project;
        projectRepository.changeStatus(id, userId, status.getDisplayName());
        project = findById(userId, id);
        return project;
    }

    @Override
    @Transactional
    public boolean existsById(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final IProjectRepository projectRepository = getRepository();
        @NotNull int count = projectRepository.existsById(id);
        return count > 0;
    }

    @Nullable
    @Override
    @Transactional
    public Project findById(@Nullable final String userId,
                            @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final IProjectRepository projectRepository = getRepository();
        @Nullable Project project;
        project = projectRepository.findById(userId, id);
        return project;
    }

    @Override
    @SneakyThrows
    @Transactional
    public Project removeById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final IProjectRepository projectRepository = getRepository();
        @NotNull final Project project = Optional.ofNullable(findById(userId, id))
                .orElseThrow(ProjectNotFoundException::new);
        projectRepository.removeById(userId, id);
        return project;
    }

    @Override
    @SneakyThrows
    @Transactional
    public Project remove(@Nullable final String userId,
                          @Nullable final Project project) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        Optional.ofNullable(findById(project.getUser().getId(), project.getId()))
                .orElseThrow(ProjectNotFoundException::new);
        removeById(project.getUser().getId(), project.getId());
        return project;
    }

    @Override
    @SneakyThrows
    @Transactional
    public int getSize(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        @NotNull final IProjectRepository projectRepository = getRepository();
        int size = projectRepository.getSize(userId);
        return size;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        @NotNull final IProjectRepository projectRepository = getRepository();
        projectRepository.clear(userId);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public List<Project> findAll(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        @NotNull final IProjectRepository projectRepository = getRepository();
        @Nullable List<Project> projects;
        projects = projectRepository.findAll(userId);
        return projects;
    }

    @Nullable
    @Override
    @Transactional
    public List<Project> addAll(@NotNull final List<Project> projects) {
        Optional.ofNullable(projects).orElseThrow(ProjectNotFoundException::new);
        @NotNull final IProjectRepository projectRepository = getRepository();
        for (Project p : projects) {
            projectRepository.add(p);
        }
        return projects;
    }

    @Nullable
    @Override
    @Transactional
    public List<Project> removeAll(@Nullable final List<Project> projects) {
        Optional.ofNullable(projects).orElseThrow(ProjectNotFoundException::new);
        @NotNull final IProjectRepository projectRepository = getRepository();
        for (Project p : projects) {
            projectRepository.removeById(p.getUser().getId(), p.getId());
        }
        return projects;
    }

    @Override
    @SneakyThrows
    @Transactional
    public Task bindTaskToProject(@Nullable final String userId,
                                  @Nullable final String projectId,
                                  @Nullable final String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new IdEmptyException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        @Nullable Task task;
        @NotNull final ITaskRepository taskRepository = context.getBean(ITaskRepository.class);
        if (projectRepository.existsById(projectId) < 1) throw new ProjectNotFoundException();
        Optional.ofNullable(taskRepository.findById(userId, taskId)).orElseThrow(TaskNotFoundException::new);
        taskRepository.bindTaskToProject(taskId, projectId, userId);
        task = taskRepository.findById(userId, taskId);
        return task;
    }

}
