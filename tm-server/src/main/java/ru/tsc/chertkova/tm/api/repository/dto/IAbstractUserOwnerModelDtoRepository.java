package ru.tsc.chertkova.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.dto.model.AbstractUserOwnerModelDTO;

import java.util.List;

public interface IAbstractUserOwnerModelDtoRepository<M extends AbstractUserOwnerModelDTO> extends IAbstractDtoRepository<M> {

    void add(@NotNull String userId, @NotNull M model);

    void clear(@NotNull String userId);

    @NotNull
    List<M> findAll(@NotNull String userId);

    @Nullable
    M findById(@NotNull String userId, @NotNull String id);

    int getSize(@NotNull String userId);

    void removeById(@NotNull String userId, @NotNull String id);

    void update(@NotNull String userId, @NotNull M model);

}
